using System;
using BankingChallenge.Services;
using BankingChallenge.Models;
using BankingChallenge.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace BankingChallengeUnitTests
{
    public class CalculatePaymentServiceUnitTests
    {
        public double Loan_amount;
        public int Duration_of_loan;
        public double Anual_interest_rate;
        public double Admin_fee;

        [Fact]
        public void TestCalculatePaymentNull()
        {
            var calculatePaymentService = new CalculatePaymentService();

            var result = calculatePaymentService.CalculatePayment(null);

            Assert.Null(result);
        }

        [Fact]
        public void TestCalculatePaymentNoValues()
        {
            var calculatePaymentService = new CalculatePaymentService();

            var result = calculatePaymentService.CalculatePayment(new Payment());

            Assert.Null(result);
        }

        [Fact]
        public void TestCalculatePaymentInvalidValues()
        {
            var calculatePaymentService = new CalculatePaymentService();

            Payment payment = new Payment();
            payment.Loan_amount= -1;
            payment.Duration_of_loan = 0;
            payment.Anual_interest_rate = 3;
            payment.Admin_fee = 2;

            var result = calculatePaymentService.CalculatePayment(new Payment());

            Assert.Null(result);
        }

        [Fact]
        public void TestCalculateMonthlyPaymentSuccess()
        {
            var calculatePaymentService = new CalculatePaymentService();

            Loan_amount = 500000;
            Anual_interest_rate = 0.05;
            Duration_of_loan = 120;

            var expectResult = 5303.28;
            var result = calculatePaymentService.CalculateMonthlyPayment(Loan_amount, Anual_interest_rate, Duration_of_loan);

            Assert.Equal(Math.Round(result, 2), expectResult);
        }

        [Fact]
        public void TestTotalInterestedRateSuccess()
        {
            var calculatePaymentService = new CalculatePaymentService();

            Loan_amount = 500000;
            Anual_interest_rate = 0.05;
            Duration_of_loan = 120;

            var expectResult = 136393.09;
            var result = calculatePaymentService.CalculateTotalInterestedRate(Loan_amount, Anual_interest_rate, Duration_of_loan);

            Assert.Equal(Math.Round(result, 2), expectResult);
        }

        [Fact]
        public void TestAdministrationFeeSuccess()
        {
            var calculatePaymentService = new CalculatePaymentService();

            Admin_fee = 0.01;
            Loan_amount = 500000;

            var expectResult = 5000;
            var result = calculatePaymentService.CalculateAdministrationFee(Admin_fee, Loan_amount);

            Assert.Equal(result, expectResult);
        }
    }
}
