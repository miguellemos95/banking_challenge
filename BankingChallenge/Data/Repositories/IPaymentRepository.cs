using BankingChallenge.Models;
using System.Collections.Generic;
using System;

namespace BankingChallenge.Data.Repositories
{
    public interface IPaymentRepository
    {
        Payment GetPayment(Guid id);
        IEnumerable<Payment> GetPayments();
        void CreatePayment(Payment payment);
        void UpdatePayment(Payment payment);
    }
}