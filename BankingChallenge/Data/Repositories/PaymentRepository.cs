using BankingChallenge.Models;
using BankingChallenge.Services;
using BankingChallenge.Dtos;
using System.Collections.Generic;
using System;
using System.Linq;

namespace BankingChallenge.Data.Repositories
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly List<Payment> payments = new ()
        {
            new Payment() {Loan_amount = 500000, Duration_of_loan = 120, Anual_interest_rate = 0.05, Admin_fee = 0.01},
            new Payment() {Loan_amount = 300000, Duration_of_loan = 130, Anual_interest_rate = 0.05, Admin_fee = 0.01},
            new Payment() {Loan_amount = 700000, Duration_of_loan = 140, Anual_interest_rate = 0.06, Admin_fee = 0.02},
            new Payment() {Loan_amount = 600000, Duration_of_loan = 160, Anual_interest_rate = 0.07, Admin_fee = 0.02}
        };

        public void CreatePayment(Payment payment)
        {
           payments.Add(payment);
        }

        public Payment GetPayment(Guid id)
        {
            return payments.Where(payment => payment.Id == id).SingleOrDefault(); 
        }

        public void UpdatePayment(Payment payment)
        {
            var index = payments.FindIndex(existingPayment => existingPayment.Id == payment.Id); 
            payments[index] = payment;
        }

        public IEnumerable<Payment> GetPayments()
        {
            return payments;
        }
    }
}
        