using AutoMapper;
using BankingChallenge.Dtos;
using BankingChallenge.Models;

namespace BankingChallenge.Profiles
{
    public class PaymentProfile : Profile
    {
        public PaymentProfile()
        {
            CreateMap<Payment, GetPaymentDto>();
            CreateMap<CreatePaymentDto, Payment>();
        }
    }
}