using System;

namespace BankingChallenge.Dtos
{
    public class GetPaymentDto
    {
        public Guid Id { get; set; }
        public double Loan_amount { get; set; }
        public int Duration_of_loan { get; set; }
        public double Anual_interest_rate {get; set;}
        public double Admin_fee {get; set;}
        public double Monthly_payment {get; set;}
        public double Total_interested_rate {get; set;}
        public double Loan_admin_fee {get; set;}
    }
}