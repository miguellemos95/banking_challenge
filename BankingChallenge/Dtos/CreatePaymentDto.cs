using System.ComponentModel.DataAnnotations;

namespace BankingChallenge.Dtos
{
    public class CreatePaymentDto
    {
        public double Loan_amount { get; set; }
        public int Duration_of_loan { get; set; }
        public double Anual_interest_rate {get; set;}
        public double Admin_fee {get; set;}
    }
}