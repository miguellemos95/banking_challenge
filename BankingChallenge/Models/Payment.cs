using System;
using BankingChallenge.Services;

namespace BankingChallenge.Models
{
    public class Payment
    {
        public Guid Id { get; set; }
        public double Loan_amount { get; set; }
        public int Duration_of_loan { get; set; }
        public double Anual_interest_rate {get; set;}
        public double Admin_fee {get; set;}
        public double Monthly_payment {get; set;}
        public double Total_interested_rate {get; set;}
        public double Loan_admin_fee {get; set;}

        public bool CheckCalc()
        {
            if(Monthly_payment == 0 && Total_interested_rate == 0 && Loan_admin_fee == 0){
                return true;
            }
            return false;
        }

        public void UpdatePaymentWithCalcValues(Payment PaymentCopy)
        {
            Id = Guid.NewGuid();
            Monthly_payment = PaymentCopy.Monthly_payment;
            Total_interested_rate = PaymentCopy.Total_interested_rate;
            Loan_admin_fee = PaymentCopy.Loan_admin_fee;
        }
    }
}