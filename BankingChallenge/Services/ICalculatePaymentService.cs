using BankingChallenge.Models;
using BankingChallenge.Dtos;

namespace BankingChallenge.Services
{
    public interface ICalculatePaymentService
    {
        Payment CalculatePaymentDTO(CreatePaymentDto createPaymentDto);
        Payment CalculatePayment(Payment Payment);
    }
}