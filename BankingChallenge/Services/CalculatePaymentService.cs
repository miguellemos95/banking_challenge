using System;
using BankingChallenge.Models;
using BankingChallenge.Dtos;

namespace BankingChallenge.Services
{
    public class CalculatePaymentService : ICalculatePaymentService
    {
        public CalculatePaymentService(){}
        public Payment CalculatePaymentDTO(CreatePaymentDto CreatePaymentDto)
        {
            double _Monthly_payment = CalculateMonthlyPayment(CreatePaymentDto.Loan_amount, CreatePaymentDto.Anual_interest_rate, CreatePaymentDto.Duration_of_loan);
            double _Total_interested_rate = CalculateTotalInterestedRate(CreatePaymentDto.Loan_amount, CreatePaymentDto.Anual_interest_rate, CreatePaymentDto.Duration_of_loan);
            double _Loan_admin_fee = CalculateAdministrationFee(CreatePaymentDto.Admin_fee, CreatePaymentDto.Loan_amount);

            return new Payment
            {
                Loan_amount = CreatePaymentDto.Loan_amount,
                Duration_of_loan = CreatePaymentDto.Duration_of_loan,
                Anual_interest_rate = CreatePaymentDto.Anual_interest_rate,
                Admin_fee = CreatePaymentDto.Admin_fee,
                Monthly_payment = _Monthly_payment,
                Total_interested_rate = _Total_interested_rate,
                Loan_admin_fee = _Loan_admin_fee
            };
        }

        // This method is just used to calculate payments for default created payments objects in repository
        public Payment CalculatePayment(Payment Payment)
        {
            if(Payment==null || Payment.Loan_amount <= 0 ||  Payment.Anual_interest_rate <= 0 || Payment.Duration_of_loan <= 0 || 
                Payment.Admin_fee < 0 ||  Payment.Loan_amount <= 0)
            {
                return null;
            }
            double _Monthly_payment = CalculateMonthlyPayment(Payment.Loan_amount, Payment.Anual_interest_rate, Payment.Duration_of_loan);
            double _Total_interested_rate = CalculateTotalInterestedRate(Payment.Loan_amount, Payment.Anual_interest_rate, Payment.Duration_of_loan);
            double _Loan_admin_fee = CalculateAdministrationFee(Payment.Admin_fee, Payment.Loan_amount);

            return new Payment
            {
                Loan_amount = Payment.Loan_amount,
                Duration_of_loan = Payment.Duration_of_loan,
                Anual_interest_rate = Payment.Anual_interest_rate,
                Admin_fee = Payment.Admin_fee,
                Monthly_payment = _Monthly_payment,
                Total_interested_rate = _Total_interested_rate,
                Loan_admin_fee = _Loan_admin_fee
            };
        }

        public double CalculateMonthlyPayment(double Loan_amount, double Anual_interest_rate, double Duration_of_loan)
        {
            double Month_interest_rate = Anual_interest_rate / 12;
            
            double Monthly_payment = Loan_amount * (Month_interest_rate * Math.Pow(1 + Month_interest_rate, Duration_of_loan)) / (Math.Pow(1 + Month_interest_rate, Duration_of_loan) - 1);
            
            return Monthly_payment;
        }

        public double CalculateTotalInterestedRate(double Loan_amount, double Anual_interest_rate, double Duration_of_loan)
        {
            double Total_payment = Duration_of_loan * CalculateMonthlyPayment(Loan_amount, Anual_interest_rate, Duration_of_loan) - Loan_amount;
        
            return Total_payment;
        }

        public double CalculateAdministrationFee(double Admin_fee, double Loan_amount)
        {      
            double Admin_fee_loan;
            double Min_admin_fee = 10000;

            if(Admin_fee * Loan_amount < Min_admin_fee)
            {
                Admin_fee_loan = Admin_fee * Loan_amount;
            }
            else
            {
                Admin_fee_loan = Min_admin_fee;
            }

            return Admin_fee_loan;
        }
    }
}