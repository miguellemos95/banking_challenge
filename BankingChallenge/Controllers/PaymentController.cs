using BankingChallenge.Models;
using BankingChallenge.Services;
using BankingChallenge.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using BankingChallenge.Dtos;
using AutoMapper;

namespace BankingChallenge.Controllers
{   
    [ApiController]
    [Route("payments")]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentRepository _PaymentRepository;
        private readonly ICalculatePaymentService _CalculatePaymentService;
        private readonly IMapper _Mapper;

        public PaymentController(IPaymentRepository PaymentRepository, ICalculatePaymentService CalculatePaymentService, IMapper Mapper)
        {
            _PaymentRepository = PaymentRepository;
            _CalculatePaymentService = CalculatePaymentService;
            _Mapper = Mapper;
        }

        // GET /Payments
        [HttpGet]
        public ActionResult<GetPaymentDto> GetPayments()
        {
            var Payments = _PaymentRepository.GetPayments();

            // This cycle is just used to calculate payments for default created payments objects in repository
            foreach(var payment in Payments)
            {
                if(payment.CheckCalc())
                {
                    Payment Payment1 =_CalculatePaymentService.CalculatePayment(payment);
                    
                    payment.UpdatePaymentWithCalcValues(Payment1);
                }
            }
            
            return Ok(_Mapper.Map<IEnumerable<GetPaymentDto>>(Payments));
        }


        // GET /Payments/{id}
        [HttpGet("{id}")]
        public ActionResult<GetPaymentDto> GetPayment(Guid id)
        {
            Payment Payment = _PaymentRepository.GetPayment(id);

            if (Payment is null)
            {
                return NotFound();
            }

            return Ok(_Mapper.Map<GetPaymentDto>(Payment));
        }

        // POST /Payments
        [HttpPost]
        public ActionResult<GetPaymentDto> CreatePayment(CreatePaymentDto CreatePaymentDto)
        {
            Payment Payment = _CalculatePaymentService.CalculatePaymentDTO(CreatePaymentDto);

            if (Payment is null)
            {
                return NotFound();
            }

            Payment.Id = Guid.NewGuid();

            _PaymentRepository.CreatePayment(Payment);

            return Ok(_Mapper.Map<GetPaymentDto>(Payment));
        }
    }
}